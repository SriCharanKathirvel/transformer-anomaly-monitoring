### File sensor setup    

This will configure the hardware connection

**Sensor** : chosen-zmpt101b

**ADC** : chosen-ADS1015-12 bit ADC

**Developement board** : rasberry pi 3

These all are supported by shunya APIs

Tell shunya interfaces hardware connection by running below command
```
 sudo vi /etc/shunya/interfaces/config.yaml
```

Telling Hardware connections to Shunya Interfaces via Sensor ID's and Connection ID's.
```
pin 3: [7.1]
pin 4: null
pin 5: [7.2]     
```
This will configure that <Sensor 7>.<Sensor pin 1> is connected to pin 3 and <Sensor 7>.<Sensor pin 2> is connected to pin 5 of rasberry pi 3.*/

![image](rpi.jpg)
