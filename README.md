# Transformer Anomaly Monitoring

## Introduction

Transformers are electric machines designed to move from one voltage level to another from an electromagnetic relationship between their primary and secondary. They can be used to increase the tension, decrease it or replicate it. Therefore it is necessary to reduce interventions and avoid permanent damage in anticipation of failures.

![](https://i.ytimg.com/vi/GS9qipIgLAU/maxresdefault.jpg)

This project helps in real-time monitoring the voltage waveform of the transformer and detect if any anomalies occur

[Project Planning Sheet](https://docs.google.com/spreadsheets/d/1r6Pmx3jKFT__5a5TeQcnn-nX2RYdCXshrYZ-1UonHF4/edit?usp=sharing)

## Architecture

![](Extras/architecture.jpg)

## About the project

* **This is an opensource project making it easy for anyone to contribute**
* Myself **Kowsyap Pranay**(@kowsyap), maintainer of the project

## How to contribute?
* Take up (select) the task from issue
* Comment in the issue that you are doing this task
* Task will be assigned to you with a deadline
* Fork and clone the project repository
* Do the task(code,documentaion or research) in respective directory of your forked repository
* Test the code in Shunya Docker [link](https://hub.docker.com/r/shunyaos/shunya-armv7)
* Create a merge request
* Maintainer review the code and merge it
* Comment in respective issues regarding doubts 

## Tasks and subtasks

1. Data Acquisition
- [Study of sensors](https://gitlab.com/iotiotdotin/project-internship/transformer-anomaly-monitoring/-/issues/1) (closed)
- [Setup sensor to transformer](https://gitlab.com/iotiotdotin/project-internship/transformer-anomaly-monitoring/-/issues/2) (closed)
- [Read data from sensor](https://gitlab.com/iotiotdotin/project-internship/transformer-anomaly-monitoring/-/issues/3) (closed)
- [Read Timestamp](https://gitlab.com/iotiotdotin/project-internship/transformer-anomaly-monitoring/-/issues/15) (closed)
2. Connectivity
- [Configuration JSON](https://gitlab.com/iotiotdotin/project-internship/transformer-anomaly-monitoring/-/issues/4)
- [Configure WiFi](https://gitlab.com/iotiotdotin/project-internship/transformer-anomaly-monitoring/-/issues/5) (closed)
- [Configure Ethernet](https://gitlab.com/iotiotdotin/project-internship/transformer-anomaly-monitoring/-/issues/6) (closed)
3. Data Transfer
- [Convert data to JSON](https://gitlab.com/iotiotdotin/project-internship/transformer-anomaly-monitoring/-/issues/7) 
- [MQTT](https://gitlab.com/iotiotdotin/project-internship/transformer-anomaly-monitoring/-/issues/8) 
4. InfluxDB
- [Study of InfluxDB](https://gitlab.com/iotiotdotin/project-internship/transformer-anomaly-monitoring/-/issues/9) (closed)
- [InfluxDB setup](https://gitlab.com/iotiotdotin/project-internship/transformer-anomaly-monitoring/-/issues/10) (closed)
- [Send data to InfluxDB](https://gitlab.com/iotiotdotin/project-internship/transformer-anomaly-monitoring/-/issues/11) (closed)
5. Grafana
- [Study of Grafana](https://gitlab.com/iotiotdotin/project-internship/transformer-anomaly-monitoring/-/issues/12) (closed)
- [Connect InfluxDB to Grafana](https://gitlab.com/iotiotdotin/project-internship/transformer-anomaly-monitoring/-/issues/16) (closed)
- [Setup Grafana](https://gitlab.com/iotiotdotin/project-internship/transformer-anomaly-monitoring/-/issues/13) (closed)
6. Battery
- [Study of Battery Indicator](https://gitlab.com/iotiotdotin/project-internship/transformer-anomaly-monitoring/-/issues/22) (asssigned)
- [Read Battery Voltage](https://gitlab.com/iotiotdotin/project-internship/transformer-anomaly-monitoring/-/issues/23) (assigned)
7. Anomaly Detection
8. Alert
- [Alarm](https://gitlab.com/iotiotdotin/project-internship/transformer-anomaly-monitoring/-/issues/14)
 

Tasks which are completed will get closed. There is chance of adding more tasks based on market requirement.

## Active Contributors

* Subhad (@subhad) **Domain Lead**
* Manisha Kumari (@Manisha6201)
* Janani (@JananiB)
* Sri Charan (@SriCharanKathirvel)
* jaisree (@Jaissri)
## Acknowledgement

* Subhad - Ethernet Configuration, Grafana (study, setup influxdb to grafana and data representation in dashboards)
* Manisha - UNIX Timestamp, Sensor (research, setup & read data)
* Janani - WiFi Configuration, Influxdb (study, setup and send data)

**Thank you for your contribution in this project**

