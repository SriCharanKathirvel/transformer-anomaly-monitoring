/**@file timestamp.c
 * @brief this program reads the timestamp
 * @author manisha kumari
**/

/*library files*/
#include<stdio.h>
#include<time.h>   /* library time which provide time delay function*/

/*function declaration return type int*/
time_t get_timestamp(){
return  time(NULL);  /*returns timestamp value to main*/
}

int main()  /*same as void main() but return type is integer*/
{
time_t timestamp; //declare variable
timestamp=get_timestamp(); //call the function
printf("Current UNIX Timestamp is %ld\n",timestamp);  //display the UNIX timestamp
return(0);  /*returns 0 if program runs successfully*/
}








